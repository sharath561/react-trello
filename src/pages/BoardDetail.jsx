import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { experimentalStyled as styled } from "@mui/material/styles";
import Stack from "@mui/material/Stack";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from '@mui/material/Typography';
import CircularIndeterminate from "../components/Loader";
import { getAllLists } from "../apis/lists/getAllLists";
import { getAllCards } from "../apis/cards/getAllCards";
import { createNewList } from "../apis/lists/createNewList";
import { deleteCards } from "../apis/cards/deleteCards";
import { deleteLists } from "../apis/lists/deleteLists";
import createNewCard from "../apis/cards/createNewCard";
import Lists from "../components/Lists";
import Cards from "../components/Cards";
import CustomizedSnackbars from "../components/ToastMessage";
import ToggleInputButton from "../toggleButtons.jsx/ToggleInputButton";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#F1F2F4",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  width: 300,
  fontWeight: 900,
  color: "black",
  flexShrink: 0,
}));

const BoardDetail = () => {
  const { boardId, name } = useParams();
  const [listsData, setListsData] = useState([]);
  const [cardsData, setCardsData] = useState({});
  const [listName, setListName] = useState("");
  const [cardName, setCardName] = useState("");
  const [errorStatus, setErrorStatus] = useState({
    loading: true,
    error: null,
  });

  useEffect(() => {
    fetchLists(boardId);
  }, [boardId]);

  const fetchLists = async (boardId) => {
    try {
      const getListData = await getAllLists(boardId);
      setListsData(getListData);
      setErrorStatus({ loading: false, error: null });

      const promises = getListData.map((list) => fetchCards(list.id));
      const cardsDataResult = await Promise.all(promises);

      const newcardsData = {};
      getListData.forEach((list, index) => {
        newcardsData[list.id] = cardsDataResult[index];
      });
      setCardsData(newcardsData);
    } catch (error) {
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const fetchCards = async (listId) => {
    try {
      const getAllCardsData = await getAllCards(listId);
      return getAllCardsData;
    } catch (error) {
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const createLists = async (listName) => {
    try {
      const createNewListData = await createNewList(listName, boardId);
      setListsData([...listsData, createNewListData]);
      setCardsData((prevCardsData) => ({
        ...prevCardsData,
        [createNewListData.id]: [],
      }));
    } catch (error) {
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const createCards = async (cardName, listId) => {
    try {
      const createCardsData = await createNewCard(cardName, listId);
      setCardsData((prevCardsData) => ({
        ...prevCardsData,
        [listId]: [...prevCardsData[listId], createCardsData],
      }));
    } catch (error) {
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const handleDeleteCards = async (cardId, listId) => {
    try {
      await deleteCards(cardId);
      setCardsData((prevCardsData) => ({
        ...prevCardsData,
        [listId]: prevCardsData[listId].filter((card) => card.id !== cardId),
      }));
    } catch (error) {
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const handleDeleteLists = async (listId) => {
    try {
      await deleteLists(listId);
      setListsData(listsData.filter((list) => list.id !== listId));
    } catch (error) {
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const handleInputChange = (event) => {
    setListName(event.target.value);
  };

  const handleInputCardChange = (event) => {
    setCardName(event.target.value);
  };

  const handleListCreate = (event) => {
    event.preventDefault();
    let nameList = listName.trim();

    if (nameList) {
      createLists(nameList);
      setListName("");
    }
  };

  const handleCardCreate = (event, listId) => {
    event.preventDefault();
    let nameCard = cardName.trim();
    if (nameCard) {
      createCards(nameCard, listId);
      setCardName("");
    }
  };

  if (errorStatus.loading) {
    return <CircularIndeterminate />;
  }
  if (errorStatus.error) {
    return (
      <>
        <Typography sx={{ fontSize: 30, textAlign: "" }}>
          {errorStatus.error}
        </Typography>
        <CustomizedSnackbars message={errorStatus.error} type={"error"} />
      </>
    );
  }

  return (
    <Container maxWidth="xl">
      <Box
        sx={{ marginTop: 3, overflowX: "auto", width: "100%", height: "90vh" }}
      >
        <Typography
          sx={{
            fontSize: 30,
            marginBottom: 3,
            color: "#1976D2",
            fontWeight: 1000,
          }}
        >
          {name}
        </Typography>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="flex-start"
          alignItems="flex-start"
        >
          {listsData.map((list) => {
            return (
              <Item key={list.id}>
                <Lists
                  list={list}
                  onDeleteLists={() => handleDeleteLists(list.id)}
                />

                {cardsData[list.id] &&
                  cardsData[list.id].map((card) => (
                    <Cards
                      card={card}
                      onDeleteCards={() => handleDeleteCards(card.id, list.id)}
                    />
                  ))}

                <ToggleInputButton
                  onCreate={(event) => handleCardCreate(event, list.id)}
                  onInputChange={handleInputCardChange}
                  value={"Card"}
                />
              </Item>
            );
          })}

          <ToggleInputButton
            onCreate={(event) => handleListCreate(event)}
            onInputChange={handleInputChange}
            value={"List"}
          />
        </Stack>
      </Box>
      <CustomizedSnackbars
        message={"Lists Fetched Successfully"}
        type={"success"}
      />
    </Container>
  );
};

export default BoardDetail;
