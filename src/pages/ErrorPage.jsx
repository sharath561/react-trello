import { Typography } from '@mui/material'
import React from 'react'

const ErrorPage = () => {
  return (
    <Typography sx={{fontSize:30,fontWeight:700,textAlign:"center",marginTop:10}}>
        404 Page not found
    </Typography>
  )
}

export default ErrorPage