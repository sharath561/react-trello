import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import { Button, Typography } from "@mui/material";
import CircularIndeterminate from "../components/Loader";
import { createNewBoard } from "../apis/board/createNewBoard";
import { getAllBoards } from "../apis/board/getAllBoards";
import Boards from "../components/Boards";
import CustomizedSnackbars from "../components/ToastMessage";
import DialogBox from "../components/DialogBox";

const HomePage = () => {
  const [boardsData, setBoardsData] = useState([]);
  const [errorStatus, setErrorStatus] = useState({
    loading: true,
    error: null,
  });

  useEffect(() => {
    fetchBoards();
  }, []);

  const fetchBoards = async () => {
    try {
      const boardsData = await getAllBoards();
      setBoardsData(boardsData);
      setErrorStatus({ loading: false, error: null });
    } catch (error) {
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const createBoard = async (boardName) => {
    try {
      setErrorStatus({ loading: true, error: null });
      const createBoardData = await createNewBoard(boardName);
      setBoardsData((prevData) => [...prevData, createBoardData]);
      setErrorStatus({ loading: false, error: null });
    } catch (error) {
      
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const  handleName = (boardName) => {
    if (boardName) {
      createBoard(boardName);
    }
  }

  if (errorStatus.loading) {
    return <CircularIndeterminate />;
  }
  if (errorStatus.error) {
    return (
      <>
        <Typography sx={{ fontSize: 30, textAlign: "" }}>
          {errorStatus.error}
        </Typography>
        <CustomizedSnackbars
          message={errorStatus.error}
          type={"error"}
        />
      </>
    );
  }

  return (
    <div>
      <Container maxWidth="xl">
        <Box sx={{ flexGrow: 1 }}>
          <Typography sx={{ fontSize: 30, fontWeight: 900, marginTop: 3 }}>
            Boards
          </Typography>

          <Grid
            container
            spacing={2}
            columns={{ xs: 4, sm: 8, md: 12 }}
            sx={{ marginTop: 2 }}
          >
            <Grid item xs={3} sm={4} md={3}>
              <DialogBox onCreateBoardName={handleName} />
            </Grid>
            {boardsData.length > 0 ? (
              boardsData.map((data) => <Boards data={data} key={data.id} />)
            ) : (
              <Typography>
                No Boards Are Available. Create a New Board
              </Typography>
            )}
          </Grid>
        </Box>
      </Container>
      <CustomizedSnackbars
        message={"Boards Fetched Successfully"}
        type={"success"}
      />
    </div>
  );
};

export default HomePage;
