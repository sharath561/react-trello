import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import { Box } from "@mui/material";
import CircularIndeterminate from "../components/Loader";
import { fetchCheckLists } from "../apis/checkList/getAllCheckLists";
import { getAllCheckItems } from "../apis/checkList/getAllCheckItems";
import { createNewCheckList } from "../apis/checkList/createNewCheckList";
import { createNewCheckItems } from "../apis/checkItems/createNewCheckItems";
import { deleteTheCheckList } from "../apis/checkList/deleteTheCheckList";
import { deleteTheCheckItems } from "../apis/checkItems/deleteTheCheckItems";
import updateCheckItems from "../apis/checkItems/updateCheckItems";
import CheckList from "../components/CheckList";
import CheckItems from "../components/CheckItems";
import CustomizedSnackbars from "../components/ToastMessage";
import ToggleInputButton from "../toggleButtons.jsx/ToggleInputButton";

export default function CheckListDialougBox({ cardName, cardId }) {
  const [open, setOpen] = React.useState(false);
  const [scroll, setScroll] = React.useState("paper");
  const [checkListName, setCheckListName] = React.useState("");
  const [checklistsData, setCheckListsData] = React.useState([]);
  const [checkItemName, setCheckItemName] = React.useState("");
  const [chekItemsData, setCheckItemsData] = React.useState([]);
  const [errorStatus, setErrorStatus] = React.useState({
    loading: false,
    error: null,
  });

  const handleClickOpen = (scrollType) => async () => {
    setOpen(true);
    setScroll(scrollType);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      getCheckLists(cardId);
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open, cardId]);

  const handleInputChange = (event) => {
    setCheckListName(event.target.value);
  };

  const getCheckLists = async (cardId) => {
    try {
      const getCheckListsData = await fetchCheckLists(cardId);
      setCheckListsData(getCheckListsData);
      setErrorStatus({ loading: false, error: null });
      // getCheckListsData.forEach((checkitem) => getCheckItems(checkitem.id));
      const promises = getCheckListsData.map((checkItem) =>
        getCheckItems(checkItem.id)
      );

      const checkItemResults = await Promise.all(promises);
      const newCheckItemsData = {};
      getCheckListsData.forEach((checkItem, index) => {
        newCheckItemsData[checkItem.id] = checkItemResults[index];
      });
      setCheckItemsData(newCheckItemsData);
    } catch (error) {
      console.log(error);
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const getCheckItems = async (checklistId) => {
    try {
      const getAllCheckItemsData = await getAllCheckItems(checklistId);
      return getAllCheckItemsData;
    } catch (error) {
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const createCheckList = async (cardId, checkListName) => {
    try {
      const createCheckListData = await createNewCheckList(
        cardId,
        checkListName
      );
      setCheckListsData([...checklistsData, createCheckListData]);
      setCheckItemsData((prevcheckCards) => ({
        ...prevcheckCards,
        [createCheckListData.id]: [],
      }));
    } catch (error) {
      console.log(error);
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const createCheckItems = async (checkListName, checklistId) => {
    try {
      const newCheckListData = await createNewCheckItems(
        checklistId,
        checkListName
      );

      setCheckItemsData((prevcheckCards) => ({
        ...prevcheckCards,
        [checklistId]: [...prevcheckCards[checklistId], newCheckListData],
      }));
    } catch (error) {
      console.log(error);
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const deleteCheckList = async (checklistId) => {
    try {
      await deleteTheCheckList(checklistId, cardId);

      setCheckListsData(
        checklistsData.filter((checklist) => checklist.id !== checklistId)
      );
    } catch (error) {
      console.log(error);
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const deleteCheckItem = async (checkItemsId, checkListId) => {
    try {
      await deleteTheCheckItems(checkItemsId, cardId);

      setCheckItemsData((prevcheckCards) => ({
        ...prevcheckCards,
        [checkListId]: prevcheckCards[checkListId].filter(
          (checkitem) => checkitem.id !== checkItemsId
        ),
      }));
    } catch (error) {
      console.log(error);
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const handleCheckItemCreate = async (event, checklistId) => {
    event.preventDefault();
    const itemName = checkItemName.trim();
    if (itemName) {
      createCheckItems(itemName, checklistId);
      setCheckItemName("");
    }
  };

  const handleInputItemChange = (event) => {
    setCheckItemName(event.target.value);
  };

  const handleCreate = async () => {
    const listName = checkListName.trim();
    if (listName) {
      await createCheckList(cardId, checkListName);

      setCheckListName("");
    }
  };

  const updateCheckItemState = async (checklistId, itemsId, newState) => {
    try {
      await updateCheckItems(cardId, itemsId, newState);

      setCheckItemsData((prevcheckCards) => ({
        ...prevcheckCards,
        [checklistId]: prevcheckCards[checklistId].map((checkitem) =>
          checkitem.id === itemsId
            ? { ...checkitem, state: newState }
            : checkitem
        ),
      }));
    } catch (error) {
      console.log(error);
      setErrorStatus({ loading: false, error: error.message });
    }
  };

  const handleCheckItemChange = async (checklistId, itemsId, itemsState) => {
    const newState = itemsState === "complete" ? "incomplete" : "complete";
    await updateCheckItemState(checklistId, itemsId, newState);
  };

  const calculateProgress = (checklistId) => {
    const items = chekItemsData[checklistId] || [];
    if (items.length === 0) return 0;
    const completedItems = items.filter((item) => item.state === "complete");
    return (completedItems.length / items.length) * 100;
  };

  if (errorStatus.loading) {
    return <CircularIndeterminate />;
  }

  if (errorStatus.error) {
    return (
      <>
        <Typography sx={{ fontSize: 30, textAlign: "" }}>
          {errorStatus.error}
        </Typography>
        <CustomizedSnackbars message={errorStatus.error} type={"error"} />
      </>
    );
  }

  return (
    <React.Fragment>
      <Typography onClick={handleClickOpen("paper")}>{cardName}</Typography>

      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth="md"
        maxWidth="md"
        scroll={scroll}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle
          id="scroll-dialog-title"
          sx={{ fontWeight: 800, fontSize: 20 }}
        >
          {cardName}
        </DialogTitle>
        <DialogContent dividers={scroll === "paper"}>
          <DialogContentText
            id="scroll-dialog-description"
            ref={descriptionElementRef}
            tabIndex={-1}
          >
            <TextField
              autoFocus
              required
              margin="dense"
              id="name"
              name="checklist"
              label="Add Check List"
              type="text"
              fullWidth
              value={checkListName}
              variant="outlined"
              onChange={handleInputChange}
            />
            <Button
              variant="contained"
              onClick={handleCreate}
              sx={{ marginTop: 2, marginBottom: 2 }}
            >
              Add an checklist
            </Button>

            <hr />
            <Typography
              sx={{
                fontSize: 20,
                fontWeight: 800,
                color: "black",
                marginTop: 2,
              }}
            >
              {checklistsData.length > 0 ? "Check Lists" : "Create a List"}
            </Typography>
            <Typography>
              {checklistsData.map((checklist) => (
                <>
                  <CheckList
                    key={checklist.id}
                    checklist={checklist}
                    onDeleteCheckList={() => deleteCheckList(checklist.id)}
                    onCalculateProgress={() => calculateProgress(checklist.id)}
                  />

                  {chekItemsData[checklist.id] &&
                    chekItemsData[checklist.id]?.map((items) => (
                      <CheckItems
                        items={items}
                        onCheckItemChange={() =>
                          handleCheckItemChange(
                            checklist.id,
                            items.id,
                            items.state
                          )
                        }
                        onDeleteCheckItem={() =>
                          deleteCheckItem(items.id, checklist.id)
                        }
                      />
                    ))}
                  <ToggleInputButton
                    onCreate={(event) =>
                      handleCheckItemCreate(event, checklist.id)
                    }
                    onInputChange={handleInputItemChange}
                    value={"Item"}
                  />
                </>
              ))}
            </Typography>
          </DialogContentText>
        </DialogContent>
      </Dialog>
      <CustomizedSnackbars
        message={"CheckLists Fetched Successfully"}
        type={"success"}
      />
    </React.Fragment>
  );
}
