import React from "react";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";

const CheckItems = ({ items, onCheckItemChange, onDeleteCheckItem }) => {
  return (
    <Typography sx={{ color: "black" }}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Box>
          <Checkbox
            checked={items.state === "complete"}
            onChange={() => onCheckItemChange()}
          />
          <span
            style={{
              textDecoration:
                items.state == "complete" ? "line-through" : "none",
            }}
          >
            {items.name}
          </span>
        </Box>
        <RemoveCircleOutlineIcon
          sx={{
            marginRight: 7,
            "&:hover": {
              color: "red",
            },
            cursor: "pointer",
          }}
          onClick={() => onDeleteCheckItem()}
        />
      </Box>
    </Typography>
  );
};

export default CheckItems;
