import React from "react";
import { Box } from "@mui/material";
import Typography from "@mui/material/Typography";
import LinearWithValueLabel from "../components/ProgressBar";
import Checkbox from "@mui/material/Checkbox";
import Button from "@mui/material/Button";

const CheckList = ({ checklist, onDeleteCheckList, onCalculateProgress }) => {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          padding: 1,
        }}
      >
        <Typography sx={{ marginTop: 2, fontWeight: 700, color: "black" }}>
          <Checkbox disabled checked />
          {checklist.name}
        </Typography>
        <Button
          variant="outlined"
          color="error"
          onClick={() => onDeleteCheckList()}
        >
          Delete
        </Button>
      </Box>

      <LinearWithValueLabel
        sx={{ marginTop: 2 }}
        progress={onCalculateProgress()}
      />
    </>
  );
};

export default CheckList;
