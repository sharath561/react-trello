import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { Typography } from "@mui/material";

import LibraryAddCheckIcon from "@mui/icons-material/LibraryAddCheck";
import DeleteIcon from "@mui/icons-material/Delete";
import Box from "@mui/material/Box";
import CheckListDialougBox from "../pages/CheckListBoxPage";

const Cards = ({ card, onDeleteCards }) => {
  return (
    <>
      <Card key={card.id} sx={{ marginTop: 2 }}>
        <CardContent sx={{ cursor: "pointer" }}>
          <Typography
            variant="body2"
            sx={{
              display: "flex",
              justifyContent: "space-between",
            }}
            fontSize="15px"
          >
            <CheckListDialougBox cardName={card.name} cardId={card.id} />
            <Box>
              {card.idChecklists.length > 0 && (
                <LibraryAddCheckIcon
                  sx={{
                    backgroundColor: "success",
                    "&:hover": {
                      color: "green",
                    },
                    cursor: "pointer",
                  }}
                  fontSize="medium"
                />
              )}

              <DeleteIcon
                sx={{
                  "&:hover": {
                    color: "red",
                  },
                  cursor: "pointer",
                }}
                onClick={() => onDeleteCards()}
              />
            </Box>
          </Typography>
        </CardContent>
      </Card>
    </>
  );
};

export default Cards;
