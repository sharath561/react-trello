import * as React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";

export default function CircularIndeterminate() {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 10,
      }}
    >
      <Box>
        <CircularProgress />
        <Typography sx={{ fontSize: 20, fontWeight: 700 }}>
          Loading ....
        </Typography>
      </Box>
    </Box>
  );
}
