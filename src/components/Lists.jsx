import React from "react";
import { Typography } from "@mui/material";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";

const Lists = ({ list, onDeleteLists }) => {
  return (
    <Typography
      variant="h6"
      sx={{
        display: "flex",
        justifyContent: "space-between",
        fontWeight: 900,
      }}
    >
      {list.name}{" "}
      <HighlightOffIcon
        sx={{
          "&:hover": {
            color: "red",
          },
          cursor: "pointer",
        }}
        onClick={() => onDeleteLists()}
      />
    </Typography>
  );
};

export default Lists;
