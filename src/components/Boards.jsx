import React from "react";
import Grid from "@mui/material/Grid";
import { NavLink } from "react-router-dom";
import { experimentalStyled as styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#1976D2",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  textDecoration: "none",
  width: 300,
  height: 150,
  marginTop: "10px",
  marginBottom: "10px",
  fontSize: 20,
  color: "white",
 
}));

const Boards = ({ data }) => {
  return (
    <Grid item xs={3} sm={4} md={3} key={data.id}>
      <NavLink to={`/boards/${data.id}/${data.name}`}>
        <Item
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            boxShadow: 20,
            fontWeight: 900,
          }}
        >
          {data.name}
        </Item>
      </NavLink>
    </Grid>
  );
};

export default Boards;
