import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { Grid, Paper, styled } from "@mui/material";
import AutohideSnackbar from "./ToastMessage";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "gray",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  textDecoration: "none",
  width: 300,
  height: 150,
  marginTop: "10px",
  marginBottom: "10px",
  fontSize: 20,
  color: "white",
}));

const DialogBox = ({ onCreateBoardName }) =>{
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Item
        onClick={handleClickOpen}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          cursor: "pointer",
          boxShadow: 20,
        }}
      >
        + Create Board
      </Item>

      <Dialog
        open={open}
        onClose={handleClose}
        PaperProps={{
          component: "form",
          onSubmit: (event) => {
            event.preventDefault();
            const formData = new FormData(event.currentTarget);
            const formJson = Object.fromEntries(formData.entries());

            onCreateBoardName(formJson.name);

            handleClose();
          },
        }}
      >
        <DialogTitle>Create Trello Board</DialogTitle>
        <DialogContent>
          <DialogContentText>New Trello Board</DialogContentText>
          <TextField
            autoFocus
            required
            margin="dense"
            id="name"
            name="name"
            label="Board Name"
            type="text"
            fullWidth
            variant="outlined"
            sx={{ width: 400 }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button type="submit">Create</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
export default DialogBox
