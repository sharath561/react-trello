import axios from 'axios';
import { API_KEY, API_TOKEN } from '../../SecretFile';

export async function deleteLists(listId){
  try{
    const response = await axios.put(
        `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${API_KEY}&token=${API_TOKEN}`
      );
      return response.data
  }catch(error){
    throw new Error(error.message)
  }
}

