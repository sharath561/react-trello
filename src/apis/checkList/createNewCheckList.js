import axios from "axios";
import { API_KEY, API_TOKEN } from "../../SecretFile";

export async function createNewCheckList(cardId, checkListName) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/cards/${cardId}/checklists?name=${checkListName}&key=${API_KEY}&token=${API_TOKEN}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error.message)
  }
}

