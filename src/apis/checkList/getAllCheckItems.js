import axios from "axios";
import { API_KEY, API_TOKEN } from "../../SecretFile";

export async function getAllCheckItems(checklistId) {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${API_KEY}&token=${API_TOKEN}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error.message)
  }
}
