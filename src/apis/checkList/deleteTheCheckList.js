import axios from "axios";
import { API_KEY, API_TOKEN } from "../../SecretFile";

export async function deleteTheCheckList(checklistId,cardId) {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/cards/${cardId}/checklists/${checklistId}?key=${API_KEY}&token=${API_TOKEN}`
    );
    // return response.data
  } catch (error) {
    throw new Error(error.message)
  }
}
