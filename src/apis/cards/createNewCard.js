import axios from 'axios';

import { API_KEY, API_TOKEN } from '../../SecretFile';

export async function createNewCard (cardName, listId){
  try{
    const response = await axios.post(
        `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${API_KEY}&token=${API_TOKEN}`
      );
      return response.data
  }catch(error){
    throw new Error(error.message)
  }
}

export default createNewCard