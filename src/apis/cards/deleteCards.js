
import { API_KEY, API_TOKEN } from "../../SecretFile";
import axios from "axios";

export async function deleteCards(cardId) {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/cards/${cardId}?key=${API_KEY}&token=${API_TOKEN}`
    );
    // return response.data;
  } catch (error) {
    throw new Error(error.message)
  }
}
