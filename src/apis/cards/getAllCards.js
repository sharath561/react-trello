
import { API_KEY, API_TOKEN } from "../../SecretFile";
import axios from "axios";

export async function getAllCards(listId) {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/lists/${listId}/cards?key=${API_KEY}&token=${API_TOKEN}`
    );
    return response.data
  } catch (error) {
    throw new Error(error.message)
  }
}
