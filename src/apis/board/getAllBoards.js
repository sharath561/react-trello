import { API_KEY, API_TOKEN } from '../../SecretFile';
import axios from 'axios';

export async function getAllBoards(){
  try{
    const response = await axios.get(
        `https://api.trello.com/1/members/me/boards?fields=name,url&key=${API_KEY}&token=${API_TOKEN}`
      );
    return response.data
  }catch(error){
    throw new Error(error.message)
  }
}

