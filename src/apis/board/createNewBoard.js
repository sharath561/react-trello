
import { API_KEY, API_TOKEN } from "../../SecretFile";
import axios from "axios";

export async function createNewBoard(boardName){
  try {
    const response = await axios.post(
      `https://api.trello.com/1/boards/?name=${boardName}&key=${API_KEY}&token=${API_TOKEN}`
    );
    return response.data
  } catch (error) {
    throw new Error(error.message)
  }
};


