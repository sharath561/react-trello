import axios from "axios";
import { API_KEY, API_TOKEN } from "../../SecretFile";

export async function createNewCheckItems(checklistId,checkListName) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${API_KEY}&token=${API_TOKEN}&name=${checkListName}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error.message)
  }
}
