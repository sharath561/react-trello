import axios from 'axios';
import { API_KEY, API_TOKEN } from '../../SecretFile';

export default async function updateCheckItems(cardId, itemsId, newState){
  try{
    const response = await axios.put(
        `https://api.trello.com/1/cards/${cardId}/checkItem/${itemsId}?key=${API_KEY}&token=${API_TOKEN}&state=${newState}`
      );
  }catch(error){
    throw new Error(error.message)
  }
}
