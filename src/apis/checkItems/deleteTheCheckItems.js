import axios from "axios";
import { API_KEY, API_TOKEN } from "../../SecretFile";

export async function deleteTheCheckItems(checkItemsId, cardId) {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemsId}?key=${API_KEY}&token=${API_TOKEN}`
    );
    // return response.data;
  } catch (error) {
    throw new Error(error.message)
  }
}
