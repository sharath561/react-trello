import React, { useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Typography } from "@mui/material";
import { experimentalStyled as styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#F1F2F4",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  width: 265,
  fontWeight: 900,
  color: "black",
  flexShrink: 0,
}));

const ToggleInputButton = ({ onCreate, onInputChange, value }) => {
  const [isVisible, setIsVisible] = useState(false);

  const handleVisible = () => {
    setIsVisible(!isVisible);
  };

  const handleSubmit = (event) => {
    onCreate(event);
    handleVisible();
  };
  
  return (
    <>
      {isVisible ? (
        <Box
          component="form"
          noValidate
          autoComplete="off"
          onSubmit={(event) => handleSubmit(event)}
          sx={{ marginTop: 2 }}
        >
          <TextField
            autoFocus
            id="outlined-basic"
            label="Create a Card"
            variant="outlined"
            onChange={(event) => onInputChange(event)}
          />
          <Button
            type="submit"
            variant="contained"
            sx={{ marginTop: 1, marginLeft: 1 }}
          >
            Add a {value}
          </Button>
          <Button
            variant="text"
            sx={{ marginTop: 1, color: "black", fontSize: 20 }}
            onClick={handleVisible}
          >
            X
          </Button>
        </Box>
      ) : (
        <Item
          sx={{
            backgroundColor: "lightgrey",
            cursor: "pointer",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 1.5,
          }}
          onClick={handleVisible}
        >
          <Typography variant="body2" sx={{ fontWeight: 800 }}>
            + Add Another {value}
          </Typography>
        </Item>
      )}
    </>
  );
};

export default ToggleInputButton;
